const express = require('express')
const axios   = require('axios')
const cheerio = require('cheerio')
const redis   = require('redis')
const cors    = require('cors');

const app = express()
app.use(cors({
  origin: 'http://localhost:8080'
}));
const port = 3000;

const pLimit = require('p-limit');
const limit = pLimit(20);

const { TMDB_API_KEY, TRAKT_CLIENT_ID } = require('./api_keys.js');

const REDISHOST = process.env.REDISHOST || 'localhost';
const REDISPORT = process.env.REDISPORT || 6379;
const REDISPASS = process.env.REDISPASS || '';

const CACHE_DURATION = 60 * 60 // 1h for now

var redisClient;

// LOCAL
app.get('/search/:search', (req, res) => {
  console.log('Received request :' + req.method + ' ' + req.url);

  res.set('Access-Control-Allow-Origin', '*');
  if (req.method === 'OPTIONS') {
    res.set('Access-Control-Allow-Methods', 'GET');
    res.set('Access-Control-Allow-Headers', '*');
    console.log('Response: 204')
    return res.sendStatus(204)
  }

  var search = req.params.search || '';
  console.log('Requested: ' + search)
  if (!search) {
    console.log('Response: 400')
    return res.sendStatus(400)
  }
  
  // create and connect redis client to local instance.
  redisClient = redis.createClient(REDISPORT, REDISHOST);
  redisClient.auth(REDISPASS, (err) => { console.log("Redis AUTH : " + err)})
  redisClient.on('connect', ()  => { console.log('Redis client connected'); });
  redisClient.on('error', (err) => { console.log("Error " + err); });

  trakt_initial_request(search).then((mega_object) => {

    redisClient.get(mega_object.trakt_id, function (error, result) {
        if (error) { console.log('redisClient error'); throw error; }
        if (result) {
          console.log('Redis Hit!')
          console.log('Response: 200')
          return res.json(JSON.parse(result))
        } else {
          console.log('Redis Miss!')

          trakt_episode_structure(mega_object).then((mega_object) => {

            get_all_ratings(mega_object).then((mega_object) => {

              tmdb_parse_data(mega_object).then((mega_object) => {

                imdb_parse_data(mega_object).then((mega_object) => {

                  calculateSeasonAverages(mega_object)

                  redisClient.set(mega_object.trakt_id, JSON.stringify(mega_object), 'EX', CACHE_DURATION, redis.print);
                  console.log('Added ' + search + ' to Redis, id ' + mega_object.trakt_id)
                  console.log('Response: 200')
                  return res.json(mega_object)

                }).catch((error) => { error_handling(res, error);})
              }).catch((error) => { error_handling(res, error);})
            }).catch((error) => { error_handling(res, error);})
          }).catch((error) => { error_handling(res, error);})
        }
    });
  }).catch((error) => { error_handling(res, error);})
}
)

trakt_initial_request = (search) => {
  return new Promise((resolve, reject) => {
    axios({
      method: 'get',
      url: `https://api.trakt.tv/search/show?query=${search}`,
      headers: {
        "Content-Type": "application/json",
        "trakt-api-version": "2",
        "trakt-api-key": TRAKT_CLIENT_ID
      }
    }).then((response) => {
      var first = response.data[0].show
      var mega_object = {
        title: first.title,
        trakt_id: first.ids.trakt,
        tmdb_id: first.ids.tmdb,
        imdb_id: first.ids.imdb,
        trakt_all_rated: true,
        tmdb_all_rated: true,
        imdb_all_rated: true
      }
      resolve(mega_object)
    }).catch((error) => { 
      reject({msg: `${error.msg || 'ERROR'} - Couldn't find show for search ${search} - replying 404`, code: 404}); 
    } )        
  })
}

trakt_episode_structure = (mega_object) => {
  return new Promise((resolve, reject) => {
    axios({
      method: 'get',
      url: `https://api.trakt.tv/shows/${mega_object.trakt_id}/seasons?extended=episodes`,
      headers: {
        "Content-Type": "application/json",
        "trakt-api-version": "2",
        "trakt-api-key": TRAKT_CLIENT_ID
      }
    }).then((response) => {
      mega_object.episodes = []
      response.data.forEach((season) => {
        if (season.number > 0) {
          season.episodes.forEach((episode) => {
            mega_object.episodes.push({
              season: episode.season,
              number: episode.number,
              index: mega_object.episodes.length+1,
              title: episode.title,
              ids: {
                trakt: episode.ids.trakt,
                tmdb: episode.ids.tmdb,
                imdb: episode.ids.imdb
              },
              links: {
                trakt: `https://trakt.tv/shows/${mega_object.trakt_id}/seasons/${episode.season}/episodes/${episode.number}`,
                imdb: `https://www.imdb.com/title/${episode.ids.imdb}`
              },
              ratings: {}
            })
          })
        }
      })
      resolve(mega_object)
    }).catch((error) => reject({msg: `${error.msg || 'ERROR'} - Error while getting data structure from trakt for show ${mega_object.trakt_id}`}))    
  })
}

get_all_ratings = (mega_object) => {
  return new Promise((resolve, reject) => {
    var promises = []

    promises.push(trakt_main_rating(mega_object))
    promises.push(tmdb_main_rating(mega_object))
    
    promises.push(trakt_episode_ratings(mega_object))
    promises.push(tmdb_episode_ratings(mega_object))
    
    promises.push(imdb_ratings(mega_object))

    Promise.all(promises)
      .then(() => { resolve(mega_object) })  
      .catch((error) => { reject(error) })   
  })
}

trakt_main_rating = (mega_object) => {
  return new Promise((resolve, reject) => {
    axios({
      method: 'get',
      url: `https://api.trakt.tv/shows/${mega_object.trakt_id}/ratings`,
      headers: {
        "Content-Type": "application/json",
        "trakt-api-version": "2",
        "trakt-api-key": TRAKT_CLIENT_ID
      }
    }).then((response) => {
      mega_object.trakt_rating = response.data.rating.toFixed(1)
      resolve(mega_object)
    }).catch((error) => reject({msg: `${error.msg || 'ERROR'} - Error while getting trakt main rating for show ${mega_object.trakt_id}`}))       
  })
}

tmdb_main_rating = (mega_object) => {
  return new Promise((resolve, reject) => {
    axios.get(`https://api.themoviedb.org/3/tv/${mega_object.tmdb_id}?api_key=${TMDB_API_KEY}`)
    .then((response) => {
      mega_object.tmdb_rating = response.data.vote_average.toFixed(1)
      resolve(mega_object)
    }).catch((error) => reject({msg: `${error.msg || 'ERROR'} - Error while getting tmdb main rating for show ${mega_object.tmdb_id}`}))     
  })
}

imdb_ratings = (mega_object) => {
  redisClient.get('IMDB_'+mega_object.imdb_id, function (error, result) {
    if (error) { console.log('redisClient error'); throw error; }
    if (!result) { 
      console.log(`Warning : No IMDB data for show ${mega_object.imdb_id}`) 
    } else {
      mega_object.imdb_data = JSON.parse(result)
    }
  })
}

trakt_episode_ratings = (mega_object) => {
  return new Promise((resolve, reject) => {
    var promises = []

    mega_object.episodes.forEach((episode) => {
      promises.push(limit( () => trakt_one_episode_rating(mega_object, episode)))
    })

    Promise.all(promises)
      .then(() => resolve(mega_object))
      .catch((error) => reject({msg: `${error.msg || 'ERROR'} - Error while getting trakt episodes rating for show ${mega_object.trakt_id}`}))       
  }) 
}

trakt_one_episode_rating = (mega_object, episode) => {
  return new Promise((resolve, reject) => {
    axios({
      method: 'get',
      url: `https://api.trakt.tv/shows/${mega_object.trakt_id}/seasons/${episode.season}/episodes/${episode.number}/ratings`,
      headers: {
        "Content-Type": "application/json",
        "trakt-api-version": "2",
        "trakt-api-key": TRAKT_CLIENT_ID
      }
    }).then((response) => {
      episode.ratings.trakt = response.data.rating.toFixed(1)
      if (episode.ratings.trakt == 0) {
        mega_object.trakt_all_rated = false
      }
      resolve(mega_object)
    }).catch((error) => reject({msg: `${error.msg || 'ERROR'} - Error while getting trakt episodes rating for show ${mega_object.trakt_id}, episode ${episode.season}.${episode.number}`}))    
  })
}

tmdb_episode_ratings = (mega_object) => {
  return new Promise((resolve, reject) => {
    var all_seasons = mega_object.episodes.map((epi) => {return epi.season})
    var seasons = [...new Set(all_seasons)] // _.uniq

    mega_object.tmdb_seasons_data = {}
    var promises = []
    seasons.forEach((season) => {
      promises.push(tmdb_one_season_rating(mega_object, season))
    })
    Promise.all(promises)
      .then(() => resolve(mega_object))
      .catch((error) => reject({msg: `${error.msg || 'ERROR'} - Error while getting tmdb episodes rating for show ${mega_object.tmdb_id}`}))   
  })
}

tmdb_one_season_rating = (mega_object, season_number) => {
  return new Promise((resolve, reject) => {
    axios.get(`https://api.themoviedb.org/3/tv/${mega_object.tmdb_id}/season/${season_number}?api_key=${TMDB_API_KEY}`)
      .then((response) => {
        response.data.episodes.forEach((epi) => {
          var subdata = tmdb_episode_subdata(epi)
          if (!mega_object.tmdb_seasons_data[season_number]) {
            mega_object.tmdb_seasons_data[season_number] = []
          }
          mega_object.tmdb_seasons_data[season_number].push(subdata)
        })
        resolve(mega_object)
      }).catch((error) => { 
        if (error.response) {
          console.log(`Warning : Getting a ${error.response.status} from TMDB for show ${mega_object.tmdb_id} season ${season_number}`)
          resolve(mega_object)
        } else {
          console.log(error)
          reject({msg: `${error.msg || 'ERROR'} - Error while getting tmdb season rating for show ${mega_object.tmdb_id} season ${season_number}`})
        }
      })   
  })
}

tmdb_episode_subdata = (epi) => {
  return {
    'id': epi.id,
    'season': epi.season_number,
    'number': epi.episode_number,
    'rating': epi.vote_average,
    'img_path': epi.still_path
  }
}

tmdb_parse_data = (mega_object) => {
  return new Promise((resolve, reject) => {

    var keys = Object.keys(mega_object.tmdb_seasons_data).sort()
    var tmdb_episodes = keys.flatMap((key) => { return mega_object.tmdb_seasons_data[key] })
    mega_object.tmdb_episodes_data = tmdb_episodes

    var mega_keys = mega_object.episodes.flatMap((epi) => { return epi.ids.tmdb })
    var tmdb_keys = tmdb_episodes.flatMap((epi) => { return epi.id })

    if (mega_keys.length == tmdb_keys.length) {
      if (mega_keys.sort() == tmdb_keys.sort().toString()) {
        mega_object.tmdb_reliability = "full"
        tmdb_save_by_id(mega_object)
      } else {
        mega_object.tmdb_reliability = "uncertain"
        tmdb_save_by_order(mega_object)
      }
    } else {
      if (keys_are_semimatching(mega_keys, tmdb_keys)) {
        mega_object.tmdb_reliability = (tmdb_keys.length > mega_keys.length) ? "excess" : "incomplete"
        tmdb_save_by_id(mega_object)
      } else {
        mega_object.tmdb_reliability = "none"
      }
    }
    delete mega_object.tmdb_episodes_data
    delete mega_object.tmdb_seasons_data

    resolve(mega_object)
  })
}

tmdb_save_by_id = (mega_object) => {
  mega_object.tmdb_episodes_data.forEach((tmdb_epi) => {
    epi = mega_object.episodes.find((epi) => { return (epi.ids.tmdb == tmdb_epi.id) })
    if (epi) { 
      save_tmdb_info(epi, tmdb_epi, mega_object) 
    }
  })
}

tmdb_save_by_order = (mega_object) => {
  mega_object.tmdb_episodes_data.forEach((tmdb_epi, index) => {
    epi = mega_object.episodes[index]
    save_tmdb_info(epi, tmdb_epi, mega_object)
  })
}

save_tmdb_info = (mega_epi, tmdb_epi, mega_object) => {
  mega_epi.img_path     = `https://image.tmdb.org/t/p/w300${tmdb_epi.img_path}`,
  mega_epi.links.tmdb   = `https://www.themoviedb.org/tv/${mega_object.tmdb_id}/season/${tmdb_epi.season}/episode/${tmdb_epi.number}`,
  mega_epi.ratings.tmdb = tmdb_epi.rating.toFixed(1)
  if (mega_epi.ratings.tmdb == 0) {
     mega_object.tmdb_all_rated=false
  }
}

imdb_parse_data = (mega_object) => {
  return new Promise((resolve, reject) => {
    // reject if imdb_data is undefined
    mega_object.imdb_rating = parseFloat(mega_object.imdb_data.rating).toFixed(1)

    var mega_keys = mega_object.episodes.flatMap((epi) => { return epi.ids.imdb })
    var imdb_keys = mega_object.imdb_data.episodes.flatMap((epi) => { return epi.id })

    if (mega_keys.length == imdb_keys.length) {
      if (mega_keys.sort() == imdb_keys.sort().toString()) {
        mega_object.imdb_reliability = "full"
        imdb_save_by_id(mega_object)
      } else {
        mega_object.imdb_reliability = "uncertain"
        imdb_save_by_order(mega_object)
      }
    } else {
      if (keys_are_semimatching(mega_keys, imdb_keys)) {
        mega_object.imdb_reliability = (imdb_keys.length > mega_keys.length) ? "excess" : "incomplete"
        imdb_save_by_id(mega_object)
      } else {
        mega_object.imdb_reliability = "none"
      }
    }
    delete mega_object.imdb_data

    resolve(mega_object)
  })
}

imdb_save_by_id = (mega_object) => {
  mega_object.imdb_data.episodes.forEach((imdb_epi) => {
    epi = mega_object.episodes.find((epi) => { return (epi.ids.imdb == imdb_epi.id) })
    if (epi) { 
      save_imdb_info(epi, imdb_epi, mega_object) 
    }
  })
}

imdb_save_by_order = (mega_object) => {
  mega_object.imdb_data.episodes.forEach((imdb_epi, index) => {
    epi = mega_object.episodes[index]
    save_imdb_info(epi, imdb_epi, mega_object)
  })
}

save_imdb_info = (mega_epi, imdb_epi, mega_object) => {
  mega_epi.ratings.imdb = parseFloat(imdb_epi.rating).toFixed(1)
  if (mega_epi.ratings.imdb == 0) {
     mega_object.imdb_all_rated=false
  }
}


keys_are_semimatching = (one, two) => {
  var isOneInTwo = one.every(i => two.includes(i));
  var isTwoInOne = two.every(i => one.includes(i));
  return (isOneInTwo || isTwoInOne)
}

calculateSeasonAverages = (mega_object) => {
  mega_object.season_averages = {
    trakt: averageForSource(mega_object.episodes, 'trakt'),
    tmdb:  averageForSource(mega_object.episodes, 'tmdb'),
    imdb:  averageForSource(mega_object.episodes, 'imdb')
  }
}

averageForSource = (epis, source) => {
  if (epis) {
    var grouped = epis.reduce(function(rv, epi) {
      rv[epi['season']] = rv[epi['season']] || []
      if (epi['ratings'][source]) {
        rv[epi['season']].push(epi['ratings'][source]);
      }
      return rv;
    }, {});

    // grouped will look like {
    //   1: ["8.0", "7.5", "5.6", "4.6", "8.9"],
    //   2: ... }

    var averages = {}

    for (const k in grouped) {
      if (grouped[k].length > 0) {
        averages[k] = (grouped[k].reduce((a, b) => parseFloat(a) + parseFloat(b)) / grouped[k].length).toFixed(1)
      }
    }

    // averages will look like { 1: 6.0, 2: 7.2 ...}

    return averages
  }
}

error_handling = (res, error) => {
  console.log(error)
  status = error.code || 500
  res.status(status).send("Oh uh, something went wrong");
}

// LOCAL
app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
})