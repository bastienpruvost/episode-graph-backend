var https = require('https'),
    fs    = require('fs'),
    zlib  = require('zlib'),
    redis   = require('redis')

download_and_extract = (file) => {
  return new Promise((resolve, reject) => {
    var url = "https://datasets.imdbws.com/" + file
    var buffer = []

    https.get(url, (res) => {
      var unzip = zlib.createGunzip();
      res.pipe(unzip);

      unzip.on('data', (data) => { 
        buffer.push(data.toString()) 
      }).on("error", (er) => {
        console.log('Error while downloading file:' + url)
      }).on("end", () => {
        console.log('Downloaded : ' + file)
        resolve(buffer.join("")); 
      })
    });
  });
}

updatePercent = (index) => {
  var new_percent = parseInt(index/length*100)
  if (new_percent > percent) {
    percent = new_percent
    process.stdout.clearLine();
    process.stdout.cursorTo(0);
    process.stdout.write(new_percent + '% (' + index + ' / ' + length + ')')
  }
}

var length = 0
var percent = 0

const REDISHOST = process.env.REDISHOST || 'localhost';
const REDISPORT = process.env.REDISPORT || 6379;
const REDISPASS = process.env.REDISPASS || '';


imdbImport = (req, res) => {
  
  const redisClient = redis.createClient(REDISPORT, REDISHOST);
  redisClient.auth(REDISPASS, (err) => { console.log("Redis AUTH : " + err)})
  redisClient.on('connect', ()  => { console.log('Redis client connected'); });
  redisClient.on('error', (err) => { console.log("Error " + err); });

  download_and_extract("title.ratings.tsv.gz").then( (ratings_content) => {
    download_and_extract("title.episode.tsv.gz").then( (episode_content) => {
    
      var ratings = {}
      ratings_content.split('\n').forEach((line) => {
        var p = line.split('\t')
        ratings[p[0]]=p[1]
      })

      var json = {}
      var episode_splitted = episode_content.split('\n')
      length = episode_splitted.length
      percent = 0

      console.log('Parsing episode data. Length: ' + length)

      episode_splitted.forEach((line, index) => {

        updatePercent(index)

        var p = line.split('\t');
        if ((p[2] != '\\N' ) && (p[3] != '\\N') && (p[3] != '0')) {

          if (!json[p[1]]) {
            json[p[1]] = { episodes: [] }
          }

          json[p[1]]['episodes'].push({
            id: p[0],
            season: p[2],
            episode: p[3],
            rating: ratings[p[0]]
          })
        }
      });

      Object.keys(json).forEach((k) => {
        var data = json[k]
        data['rating'] = ratings[k]
        data['episodes'].sort(function(a, b) {          
          if (a.season === b.season) {
             return a.episode - b.episode;
          } else {
            return a.season - b.season;
          }
        })
        redisClient.set('IMDB_'+k, JSON.stringify(data));
      })

      redisClient.quit(() => {
        console.log('\nDone.')
      })
    })
  })
}

imdbImport();